const router = require("express").Router()
const Appointment = require("../models/Appointments")
const User = require("../models/Users")

router.get("/", async (req, res) => {
    try {
        const appointment = await Appointment.find().populate("user")
        res.send(appointment)
    } catch (error) {
        res.send(`error with getting user: ${error}`)
    }
})

router.get("/init", async (req, res) => {
    const Testi = new Appointment
    const user = await User.find({ name: "Vatto" })
    let response = JSON.stringify(user)
    const response2 = response.match(/"(id)":"((\\"|[^"])*)"/)
    Testi.start_time = "08:00"
    Testi.user = response2[2].toString()
    Testi.time_length = 50
    Testi.appointment_date = "2015-04-14"
    Testi.save()
    res.send("Initialized database/created test post")
})

router.post("/new", async (req, res) => {
    // ensin tarkistetaan onko käyttäjätili olemassa
    try {
        const user = JSON.stringify(await User.find({ name: req.body.name }))
        const name = user.match(/"(name)":"((\\"|[^"])*)"/)
        const idArr = user.match(/"(id)":"((\\"|[^"])*)"/)
        const id = idArr[2]
        if (name[2] === req.body.name) {
            const appointment = new Appointment
            appointment.start_time = req.body.start_time
            appointment.user = id
            appointment.time_length = req.body.time_length
            appointment.appointment_date = req.body.appointment_date
            appointment.save()
            // await User.findByIdAndUpdate({id},{}, function(err, result){
            //     if (err) {
            //         res.send(err)
            //     }
            //     if (result) {
            //         console.log(result);
            //     }
            // })
            
            //const user = User.findById(id)
            //user.appointments.push(appointment.id)
            //user.save()

            await User.updateOne(
                { _id: id },
                { $push: { appointments: appointment.id }}
            )
            res.send("Created new appointment")
        } else {
            res.send("Error")
        }
    } catch (error) {
        // res.send("User doesn't exist. Try creating it first with /api/users/create." + error)
        res.send("User doesn't exist. Try creating it first with /api/users/create")
    }
})

router.delete("/delete/:id", async (req, res) => {
    try {
        await Appointment.findByIdAndDelete(req.params.id, function (err, doc) {
            if (err) return res.send(500, { error: err })
            return res.send("Deleted appointment succesfully")
        })
    } catch (e) {
        console.log("Error in delete appointments + " + e);
        res.status(404).send(e)
    }
})

module.exports = router