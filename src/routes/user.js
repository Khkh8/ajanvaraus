const router = require("express").Router()
const User = require("../models/Users")

router.get("/", async (req, res) => {
    try {
        const user = await User.find().populate("appointments")
        res.send(user)
    } catch (error) {
        res.send(`error with getting user: ${error}`)
    }
})

router.get("/init", async (req, res) => {
    const Testi = new User
    Testi.name = "Vatto"

    Testi.save(function (err, post) {
        if (err) { return next(err) }
        res.json(201, post)
    })
})

router.post("/create", async (req, res) => {
    const user = new User
    const response = JSON.stringify(await User.find({ name: req.body.name }))
    const name = response.match(/"(name)":"((\\"|[^"])*)"/)
    try {
        if (name[2] === req.body.name) {
            res.send(`User exists already. Create a new one.`)
        }
    } catch (error) {
        try {
            user.name = req.body.name
            user.save()
            res.send("User created");
        } catch (error) {
            res.send("Error with creating new user")
        }
    }
})

module.exports = router