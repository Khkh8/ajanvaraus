const appointment = require("./appointment")
const user = require("./user")

module.exports = {
    appointment,
    user
}