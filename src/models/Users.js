const mongoose = require("mongoose")
const Appointment = require("./Appointments")

const userSchema = new mongoose.Schema({
    name:           { type: String, required: true, maxlength: 40 },
    appointments:   [{ type: mongoose.Schema.Types.ObjectId, ref: "Appointment", required: true }]
})

userSchema.set('toJSON', {
    transform: (document, returnedObject) => {
        returnedObject.id = returnedObject._id.toString()
        delete returnedObject._id
        delete returnedObject.__v
    },
})

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
      
//Export model
module.exports = mongoose.model("User", userSchema)