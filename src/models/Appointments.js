const mongoose = require("mongoose")
const User = require("./Users")

const appointmentSchema = new mongoose.Schema({
    start_time:         { type: String, required: true, maxlength: 5 },
    user:               { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
    time_length:        { type: Number, required: true },
    appointment_date:   { type: String, required: true, maxlength: 10 }
})
// appointment_date muotoa ISO8601 eli yyyy-mm-dd

appointmentSchema.set('toJSON', {
    transform: (document, returnedObject) => {
        returnedObject.id = returnedObject._id.toString();
        delete returnedObject._id;
        delete returnedObject.__v;
    },
});

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
      
//Export model
module.exports = mongoose.model('Appointment', appointmentSchema);