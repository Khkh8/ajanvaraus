require("dotenv").config()
const express = require("express")
const port = process.env.PORT
const app = express()
const bodyParser = require("body-parser")
const mongoose = require("mongoose")
const routes = require("./routes")

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

mongoose.connect(process.env.CONNECTION_STRING, {useNewUrlParser: true})
const db = mongoose.connection

db.on("error", console.error.bind(console, "connection error:"))

db.once("open", () => {
    console.log("Database connection established...");
})

app.use("/api/appointments", routes.appointment)
app.use("/api/users", routes.user)

app.get("/", (req, res) => {
    res.send("Ajanvaraussovellus")
})

app.listen(port, () => {
    console.log(`Server running on port ${port}...`)
})
